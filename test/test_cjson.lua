local inspect = require("inspect")
local json = require("cjson")

local text = "{\"message\":\"Hello\"}"

print(inspect(json))
print(inspect(json.decode(text)))
